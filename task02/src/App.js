import React, {Component} from 'react';
import './fonts/fonts.css';
import './css/style.css';
import user from './img/user.svg';
import logo from './img/logo.svg';
import menu from './img/menu.svg';
import polygon from './img/polygon.svg';
import user2 from './upload/user.jpg';
import pencil from './img/pencil.svg';

const Header = () => {
	return (
		<header>
			<a href="index.html" className="logo"><img src={logo} width="143" height="46" alt="logo"/></a>
			<button className="btn-menu"><img src={menu} width="38" height="23" alt="menu"/>
			</button>
			<div id="open-dropdown" className="user">
				<span className="lk"><img src={user} width="41" height="41" alt="user"/></span>
				<span className="open-dropdown"><img src={polygon} width="13" height="8"
				                                     alt=""/></span>
				<div className="dropdown-menu">
					<ul className="up">
						<li><a href="index.html">Главная</a></li>
						<li><a href="index.html">О программе</a></li>
						<li><a href="index.html">Календарь</a></li>
						<li><a href="index.html">FAQ</a></li>
						<li><a href="index.html">Помокод</a></li>
					</ul>
					<ul className="bottom">
						<li><a href="index.html">Мой профиль</a></li>
						<li><a href="index.html">Выйти</a></li>
						<li><a id="logIn" href="index.html">Войти</a></li>
					</ul>
				</div>
			</div>
		</header>
	)
};

const Main = () => {
	return (
		<section className="candidat">
			<div className="section_header candidat">
				<div className="menu-button">
					<div className="bar"></div>
					<div className="bar"></div>
					<div className="bar"></div>
				</div>
				<h3>Lesovaya Marri <a href="index.html" className="edit"><img src={pencil} width="35"
				                                                              height="35" alt="edit"/></a>
				</h3>
			</div>
			<div className="container flex-container">
				<div className="sidebar">
					<div className="user_photo">
						<img src={user2} width="783" height="783" alt="user photo"/>
					</div>
					<div className="status"></div>
					<div className="sidebar-menu">
						<ul className="up">
							<li><a href="index.html">Главная</a></li>
							<li><a href="index.html">О программе</a></li>
							<li><a href="index.html">Календарь</a></li>
							<li><a href="index.html">FAQ </a></li>
							<li><a href="index.html">Помокод</a></li>
						</ul>
						<ul className="bottom">
							<li><a href="index.html">Группа в Facebook</a></li>
							<li><a href="index.html">Канал Telegram</a></li>
							<li><a href="index.html">Instagram</a></li>
						</ul>
					</div>
				</div>
				<div className="contentIn">
					<h2>Личный кабинет</h2>
					<div className="content_text">
						<p className="one">
							Добро пожаловать в IamUnity. Это твой личный кабинет.
							Здесь ты узнаешь всю информацию о программе Reboot.
							Ты сможешь выбрать подходящую дату начала курса и получить ответы
							на вопросы в разделе FAQ.
						</p>
					</div>
				</div>
			</div>
		</section>
	)
};

const Footer = () => {
	return (
		<footer>
			<div className="container flex-container">
				<div className="left">
					<a href="index.html" className="logo"><img src={logo} width="143"
					                                           height="46" alt="logo"/></a>
					<p>©2017</p>
				</div>
				<div className="right">
					<ul>
						<li className="fb"><a href="index.html"><i className="fa fa-facebook" aria-hidden="true"></i></a></li>
						<li className="vk"><a href="index.html"><i className="fa fa-vk" aria-hidden="true"></i></a></li>
						<li className="google"><a href="index.html"><i className="fa fa-google-plus" aria-hidden="true"></i></a></li>
						<li className="instagram"><a href="index.html"><i className="fa fa-instagram" aria-hidden="true"></i></a></li>
					</ul>
				</div>
			</div>
		</footer>
	)
};

class App extends Component {
	render() {
		return (
			<div>
				<Header />
				<Main />
				<Footer />
			</div>
		);
	}
}

export default App;